# -*- encoding: utf-8 -*-

import os
import sys
from distutils.core import setup

from setuptools import find_packages


assert sys.version_info >= (2, 7), "Python 2.7+ required."

current_dir = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(current_dir, 'README.rst')) as readme_file:
    long_description = readme_file.read()

sys.path.insert(0, current_dir + os.sep + 'src')
from trypy import VERSION
release = ".".join(str(num) for num in VERSION)


setup(
    name='trypy_cheker',
    version=release,
    author='Tomasz Ziółkowski',
    author_email='tomasz.ziolkowski@allegro.pl',
    description="Checker for workshop tasks",
    long_description=long_description,
    keywords='',
    platforms=['any'],
    license='Apache Software License v2.0',
    packages=find_packages('src'),
    include_package_data=True,
    package_dir={'': 'src'},
    zip_safe=False,
    console=['src/trypy/console.py'],
    entry_points={
        'console_scripts': [
            'trypy-checker = trypy.console:main',
            ],
        },
    zipfile=None,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: Apache Software License',
        'Natural Language :: English',
        'Operating System :: POSIX',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows :: Windows NT/2000',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 2 :: Only',
        'Topic :: Internet :: WWW/HTTP',
        ]
)
