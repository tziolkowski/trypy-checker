# -*- encoding: utf-8 -*-
import os
import subprocess
import re
import abc
import requests


METRICS = ['uptime', 'load', 'disk']
MONITOR = 'tutorial/monitor.py'


class CheckError(RuntimeError):
    pass


class Check(object):

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def is_applicable(self, context):
        return

    @abc.abstractmethod
    def stop_propagation(self, context):
        return

    @abc.abstractmethod
    def check(self, context):
        return


class EnvCheck(Check):

    def is_applicable(self, context):
        return True

    def stop_propagation(self, context):
        return True

    def check(self, context):
        if not os.path.exists(context.get_virtual_path()):
            raise CheckError("{} not exists".format(context.get_virtual_path()))

        if not os.path.exists("{}/bin/pep8".format(context.get_virtual_path())):
            raise CheckError("pep8 not installed in your virtual env - please install it via pip")

        if not os.path.exists(context.get_source_path()):
            raise CheckError("{} not exists".format(context.get_source_path()))

        print "Environment:\nsource_path:{}\nAPI_KEY: {}\ntipboard port: {}\n".format(
            context.get_source_path(), context.get_api_key(), context.get_api_port())


class ExternalCommandCheck(Check):
    def __init__(self, command):
        self.command = command

    def is_applicable(self, context):
        return context.get_branch() in ['master', 'level-1', 'level-1-finish']

    def stop_propagation(self, context):
        return False

    def check(self, context):
        return_code = subprocess.call(
            [
                "{}/bin/{}".format(context.get_virtual_path(), self.command),
                "{}/tutorial".format(context.get_source_path())],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        if return_code:
            raise CheckError("{} fail - run it and check errors".format(self.command))


class Pep8Check(ExternalCommandCheck):
    def __init__(self):
        ExternalCommandCheck.__init__(self, 'pep8')


class NoseTestCheck(ExternalCommandCheck):
    def __init__(self):
        ExternalCommandCheck.__init__(self, 'nosetests')

    def is_applicable(self, context):
        return context.get_branch() in ['master', 'level-6', 'level-6-finish']


class CallMonitorCheck(Check):

    def stop_propagation(self, context):
        return False

    def is_applicable(self, context):
        return False

    def check(self, context):
        for metric in METRICS:
            result = subprocess.check_output(["{}/bin/python".format(context.get_virtual_path()),
                                              "{}/{}".format(context.get_source_path(), MONITOR),
                                              "--{}".format(metric)],
                                             stderr=subprocess.STDOUT)
            self.verify_call(metric, result, context)

    def verify_call(self, metric, result, context):
        pass


class ArgPassCheck(CallMonitorCheck):
    def is_applicable(self, context):
        return context.get_branch() in ['level-1', 'level-1-finish']

    def verify_call(self, metric, result, context):
        if metric not in result:
            raise CheckError("Call {} --{} return {}, but expected: {}".format(MONITOR, metric, result, metric))


class CallProcessCheck(CallMonitorCheck):
    def is_applicable(self, context):
        return context.get_branch() in ['level-2', 'level-2-finish']

    def verify_call(self, metric, result, context):
        markers = {
            'uptime': " up ",
            'load': "load average",
            'disk': 'Filesystem'
        }
        if markers[metric] not in result:
            raise CheckError("Call {} --{}, but not found marker: <{}> in result: {}".format(
                MONITOR, metric, markers[metric], result))


class ParseMetricCheck(CallMonitorCheck):
    def is_applicable(self, context):
        return context.get_branch() in ['level-3', 'level-3-finish']

    def verify_call(self, metric, result, context):
        markers = {
            'uptime': r"uptime: [0-9:]+.*",
            'load': r"load: [0-9.]+",
            'disk': r"disk: [0-9]+/[0-9]+"
        }
        if not re.match(markers[metric], result):
            raise CheckError("Call {} --{}, but cannot match /{}/ to result: {}".format(
                MONITOR, metric, markers[metric], result))


class SendMetricCheck(CallMonitorCheck):
    def is_applicable(self, context):
        return context.get_branch() in ['level-4', 'level-5', 'level-4-finish', 'level-5-finish', 'master']

    def verify_call(self, metric, result, context):
        markers = {
            'uptime': r".*uptime: (?P<data>[0-9:]+.*)",
            'load': r".*load: (?P<data>[0-9.]+)",
            'disk': r".*disk: (?P<Free>[0-9]+)/(?P<Used>[0-9]+)"
        }
        tiles = {
            'uptime': 'uptime',
            'disk': 'disk_usage',
            'load': 'load'
        }
        m = re.match(markers[metric], result)
        if not m:
            raise CheckError("Call {} --{}, but cannot match /{}/ to result: {}".format(
                MONITOR, metric, markers[metric], result))

        r = requests.get("{}{}".format(context.get_api_url(), tiles[metric]))
        try:
            if metric == 'disk':
                for key, value in r.json['data']['pie_data']:
                    if m.group(key) != str(value):
                        raise CheckError("Tipboard metric {}/{}={} is not equal printed value {}".format(
                            metric, key, value, m.group(key)))
            else:
                if m.group('data') != str(r.json['data']['just-value']):
                    raise CheckError("Tipboard metric {}={} is not equal printed value {}".format(
                        metric, r.json['data']['just-value'], m.group('data')))

        except Exception:
            raise CheckError('Inconsistent or empty tipboard data')


class Verifier(object):
    def __init__(self, check_chain):
        self.check_chain = check_chain

    def check_solution(self, context):
        result = True
        for check in self.check_chain:
            if check.is_applicable(context):
                try:
                    check.check(context)
                    print "{} Passed".format(check.__class__.__name__)
                except CheckError as check_error:
                    result = False
                    print "{} Failed with message: {}".format(check.__class__.__name__, check_error.message)
                    if check.stop_propagation(context):
                        return result
            else:
                print "{} not applicable".format(check.__class__.__name__)

        return result
