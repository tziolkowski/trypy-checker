# -*- encoding: utf-8 -*-
"""trypy-checker

Usage:
  trypy-check
  trypy-check (-h | --help)
  trypy-check (-v|--version)

Options:
  -h --help             Show this screen.
  -v --version          Show version.
"""
from docopt import docopt
from trypy.check import Verifier, EnvCheck, Pep8Check, ArgPassCheck, CallProcessCheck, ParseMetricCheck, \
    SendMetricCheck, NoseTestCheck
from trypy.context import Context


def main():
    docopt(__doc__, version='trypy-checker 0.1.0')
    Verifier([EnvCheck(),
              Pep8Check(),
              ArgPassCheck(),
              CallProcessCheck(),
              ParseMetricCheck(),
              SendMetricCheck(),
              NoseTestCheck()]).check_solution(Context())

if __name__ == "__main__":
    main()
