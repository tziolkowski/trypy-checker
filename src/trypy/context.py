# -*- encoding: utf-8 -*-
import os
import imp
import re
from git import Repo


class Context(object):
    VIRTUAL_PATH = '/tutorial-env'
    SOURCE_PATH = '/trypy'

    def __init__(self):
        self.home = os.path.expanduser('~')

    def get_virtual_path(self):
        return "{}{}".format(self.home, self.VIRTUAL_PATH)

    def get_source_path(self):
        return "{}{}".format(self.home, self.SOURCE_PATH)

    def get_branch(self):
        return str(Repo(self.get_source_path()).active_branch)

    def get_api_key(self):
        tipboard = imp.load_source('tipboard', '{}/.tipboard/settings-local.py'.format(self.home))
        return tipboard.API_KEY

    def get_api_port(self):
        m = re.match(r".*pyuser-(?P<number>[0-9]{2})", self.home)
        port = 7082
        if m:
            port = 7000 + int(m.group('number'))
        return port

    def get_api_url(self):
        return 'http://localhost:{}/api/v0.1/{}/tiledata/'.format(self.get_api_port(), self.get_api_key())

    def __str__(self):
        return "virtual_path: {}\nsource_path: {}\nbranch: {}".format(
            self.get_virtual_path(), self.get_source_path(), self.get_branch())
