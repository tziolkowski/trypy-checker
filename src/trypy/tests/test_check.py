import subprocess
from unittest import TestCase
from mock import patch, MagicMock
from trypy.check import EnvCheck, CheckError, ExternalCommandCheck, CallMonitorCheck, METRICS, ArgPassCheck, \
    CallProcessCheck, ParseMetricCheck, SendMetricCheck, Verifier

VIRTUAL_PATH = '/home/vagrant/tutorial-env'
SOURCE_PATH = '/home/vagrant/trypy'


class ContextTestCase(TestCase):

    def setUp(self):
        self.context = MagicMock()
        self.context.get_virtual_path = MagicMock(return_value=VIRTUAL_PATH)
        self.context.get_source_path = MagicMock(return_value=SOURCE_PATH)


class EnvCheckTest(ContextTestCase):

    def path_exists_side_effect(self, arg):
        return self.paths_existence[arg]

    def setUp(self):
        super(EnvCheckTest, self).setUp()

        patcher = patch('os.path.exists')
        self.path_exits_mock = patcher.start()
        self.path_exits_mock.side_effect = self.path_exists_side_effect
        self.addCleanup(patcher.stop)

    def test_should_raise_check_error_if_virtual_path_not_exits(self):
        self.paths_existence = {VIRTUAL_PATH: False}
        with self.assertRaises(CheckError):
            EnvCheck().check(self.context)

    def test_should_raise_check_error_if_pep_path_not_exits(self):
        self.paths_existence = {
            VIRTUAL_PATH: True,
            "{}/bin/pep8".format(VIRTUAL_PATH): False
        }
        with self.assertRaises(CheckError):
            EnvCheck().check(self.context)

    def test_should_raise_check_source_path_not_exits(self):
        self.paths_existence = {
            VIRTUAL_PATH: True,
            "{}/bin/pep8".format(VIRTUAL_PATH): True,
            SOURCE_PATH: False
        }
        with self.assertRaises(CheckError):
            EnvCheck().check(self.context)

    def test_should_pass_if_all_paths_exists(self):
        self.paths_existence = {
            VIRTUAL_PATH: True,
            "{}/bin/pep8".format(VIRTUAL_PATH): True,
            SOURCE_PATH: True
        }
        self.assertIsNone(EnvCheck().check(self.context))


class ExternalCommandCheckTest(ContextTestCase):

    @patch('subprocess.call')
    def test_should_raise_check_error_if_subprocess_return_non_zero_status(self, call_mock):
        call_mock.return_value = 1
        with self.assertRaises(CheckError):
            ExternalCommandCheck('test-command').check(self.context)

    @patch('subprocess.call')
    def test_should_pass_if_subprocess_return_zero_status(self, call_mock):
        call_mock.return_value = 0
        self.assertIsNone(ExternalCommandCheck('test-command').check(self.context))

    def test_should_be_applicable_to_master_and_level_one(self):
        self.context.get_branch = MagicMock(return_value='master')
        self.assertTrue(ExternalCommandCheck(self.context).is_applicable(self.context))
        self.context.get_branch = MagicMock(return_value='level-1')
        self.assertTrue(ExternalCommandCheck(self.context).is_applicable(self.context))
        self.context.get_branch = MagicMock(return_value='level-1-finish')
        self.assertTrue(ExternalCommandCheck(self.context).is_applicable(self.context))

        self.context.get_branch = MagicMock(return_value='level-2')
        self.assertFalse(ExternalCommandCheck(self.context).is_applicable(self.context))
        self.context.get_branch = MagicMock(return_value='level-2-finish')
        self.assertFalse(ExternalCommandCheck(self.context).is_applicable(self.context))


class CallMonitorCheckTest(ContextTestCase):

    @patch('subprocess.check_output', return_value=None)
    def test_should_call_external_process_and_verify_response_for_each_metric(self, check_output_mock):
        with patch.object(CallMonitorCheck, 'verify_call', return_value=None) as verify_call_mock:
            CallMonitorCheck().check(self.context)
            for metric in METRICS:
                check_output_mock.assert_any_call(["/home/vagrant/tutorial-env/bin/python",
                                                   "/home/vagrant/trypy/tutorial/monitor.py",
                                                   "--{}".format(metric)], stderr=subprocess.STDOUT)
                verify_call_mock.assert_any_call(metric, None, self.context)


class ArgPassCheckTest(ContextTestCase):
    def test_should_be_applicable_to_level_one(self):
        self.context.get_branch = MagicMock(return_value='level-1')
        self.assertTrue(ArgPassCheck().is_applicable(self.context))
        self.context.get_branch = MagicMock(return_value='level-1-finish')
        self.assertTrue(ArgPassCheck().is_applicable(self.context))

    def test_should_raise_check_error_if_metric_string_not_present_in_output(self):
        with self.assertRaises(CheckError):
            ArgPassCheck().verify_call('test-metric', 'output without metric', self.context)


class CallProcessCheckTest(ContextTestCase):
    def test_should_be_applicable_to_level_two(self):
        self.context.get_branch = MagicMock(return_value='level-2')
        self.assertTrue(CallProcessCheck().is_applicable(self.context))
        self.context.get_branch = MagicMock(return_value='level-2-finish')
        self.assertTrue(CallProcessCheck().is_applicable(self.context))

    def test_raise_check_error_if_marker_for_metrics_not_present_in_output(self):
        with self.assertRaises(CheckError):
            CallProcessCheck().verify_call('uptime', 'output without uptime marker', self.context)
        with self.assertRaises(CheckError):
            CallProcessCheck().verify_call('load', 'output without load marker', self.context)
        with self.assertRaises(CheckError):
            CallProcessCheck().verify_call('disk', 'output without disk marker', self.context)

    def test_should_pass_if_markers_for_metrics_found(self):
        check = CallProcessCheck()
        self.assertIsNone(check.verify_call('uptime', '09:21:02 up  2:21', self.context))
        self.assertIsNone(check.verify_call('load', 'load average: 0.00, 0.01, 0.05', self.context))
        self.assertIsNone(check.verify_call('disk', 'Filesystem           1K-blocks', self.context))


class ParseMetricCheckTest(ContextTestCase):
    def test_should_be_applicable_to_level_three(self):
        self.context.get_branch = MagicMock(return_value='level-3')
        self.assertTrue(ParseMetricCheck().is_applicable(self.context))
        self.context.get_branch = MagicMock(return_value='level-3-finish')
        self.assertTrue(ParseMetricCheck().is_applicable(self.context))

    def test_raise_check_error_if_metric_output_not_match_to_expected_format(self):
        with self.assertRaises(CheckError):
            ParseMetricCheck().verify_call('uptime', 'uptime: s 45', self.context)
        with self.assertRaises(CheckError):
            ParseMetricCheck().verify_call('load', 'load; 12 ', self.context)
        with self.assertRaises(CheckError):
            ParseMetricCheck().verify_call('disk', 'disk: 45:12', self.context)

    def test_should_pass_if_out_for_metrics_is_correct(self):
        check = ParseMetricCheck()
        check.verify_call('uptime', 'uptime: 2:21', self.context)
        check.verify_call('load', 'load: 0.00', self.context)
        check.verify_call('disk', 'disk: 11234/1213243', self.context)


class SendMetricCheckTest(ContextTestCase):
    def test_should_be_applicable_to_levels_fourth_and_fifth(self):
        self.context.get_branch = MagicMock(return_value='level-4')
        self.assertTrue(SendMetricCheck().is_applicable(self.context))
        self.context.get_branch = MagicMock(return_value='level-4-finish')
        self.assertTrue(SendMetricCheck().is_applicable(self.context))
        self.context.get_branch = MagicMock(return_value='level-5')
        self.assertTrue(SendMetricCheck().is_applicable(self.context))
        self.context.get_branch = MagicMock(return_value='level-5-finish')
        self.assertTrue(SendMetricCheck().is_applicable(self.context))

    def test_raise_check_error_if_metric_output_not_match_to_expected_format(self):
        with self.assertRaises(CheckError):
            SendMetricCheck().verify_call('uptime', 'uptime: s 45', self.context)
        with self.assertRaises(CheckError):
            SendMetricCheck().verify_call('load', 'load; 12 ', self.context)
        with self.assertRaises(CheckError):
            SendMetricCheck().verify_call('disk', 'disk: 45:12', self.context)

    @patch('requests.get')
    def test_raise_check_error_if_dashboard_return_incorrect_response(self, get_mock):
        get_mock.return_value = MagicMock(json='')
        with self.assertRaises(CheckError):
            SendMetricCheck().verify_call('uptime', 'uptime: 2:21', self.context)

    @patch('requests.get')
    def test_raise_check_error_if_dashboard_and_output_data_not_match(self, get_mock):
        get_mock.return_value = MagicMock(json={'data': {'just-value': '2:20'}})
        with self.assertRaises(CheckError):
            SendMetricCheck().verify_call('uptime', 'uptime: 2:21', self.context)

        get_mock.return_value = MagicMock(json={'data': {'just-value': '0.89'}})
        with self.assertRaises(CheckError):
            SendMetricCheck().verify_call('load', 'load: 0.88', self.context)

        get_mock.return_value = MagicMock(json={'data': {'pie_data': [['Free', '12'], ['Used', '15']]}})
        with self.assertRaises(CheckError):
            SendMetricCheck().verify_call('disk', 'disk: 12/18', self.context)

    @patch('requests.get')
    def test_should_pass_if_dashboard_and_output_data_match(self, get_mock):
        get_mock.return_value = MagicMock(json={'data': {'just-value': '2:20'}})
        self.assertIsNone(SendMetricCheck().verify_call('uptime', 'uptime: 2:20', self.context))

        get_mock.return_value = MagicMock(json={'data': {'just-value': '0.89'}})
        self.assertIsNone(SendMetricCheck().verify_call('load', 'load: 0.89', self.context))

        get_mock.return_value = MagicMock(json={'data': {'pie_data': [['Free', '12'], ['Used', '15']]}})
        self.assertIsNone(SendMetricCheck().verify_call('disk', 'disk: 12/15', self.context))


class VerifierTest(ContextTestCase):

    def test_should_call_check_only_if_is_applicable(self):
        check_mock_called = MagicMock()
        check_mock_not_called = MagicMock()

        applicable_check = MagicMock()
        applicable_check.is_applicable = MagicMock(return_value=True)
        applicable_check.check = check_mock_called

        non_applicable_check = MagicMock()
        non_applicable_check.is_applicable = MagicMock(return_value=False)
        non_applicable_check.check = check_mock_not_called

        Verifier([applicable_check, non_applicable_check]).check_solution(self.context)

        check_mock_called.assert_any_call(self.context)
        self.assertFalse(check_mock_not_called.called)

    def test_should_stop_processing_checks_after_stop_propagation_check_fail_and_return_false(self):
        check_mock_called = MagicMock()
        check_mock_called.side_effect = CheckError('Check not pass')
        check_mock_not_called = MagicMock()

        stop_propagation_check = MagicMock()
        stop_propagation_check.is_applicable = MagicMock(return_value=True)
        stop_propagation_check.stop_propagation = MagicMock(return_value=True)
        stop_propagation_check.check = check_mock_called

        applicable_check = MagicMock()
        applicable_check.is_applicable = MagicMock(return_value=True)
        applicable_check.check = check_mock_not_called

        self.assertFalse(Verifier([stop_propagation_check, applicable_check]).check_solution(self.context))

        check_mock_called.assert_any_call(self.context)
        self.assertFalse(check_mock_not_called.called)

    def test_should_call_continue_processing_checks_if_non_stop_propagation_check_fail_and_return_false(self):
        check_mock_called = MagicMock()
        check_mock_called.side_effect = CheckError('Check not pass')
        second_check_called = MagicMock()

        stop_propagation_check = MagicMock()
        stop_propagation_check.is_applicable = MagicMock(return_value=True)
        stop_propagation_check.stop_propagation = MagicMock(return_value=False)
        stop_propagation_check.check = check_mock_called

        applicable_check = MagicMock()
        applicable_check.is_applicable = MagicMock(return_value=True)
        applicable_check.check = second_check_called

        self.assertFalse(Verifier([stop_propagation_check, applicable_check]).check_solution(self.context))

        check_mock_called.assert_any_call(self.context)
        second_check_called.assert_any_call(self.context)

    def test_should_return_true_if_all_applicable_tests_pass(self):

        first_check = MagicMock()
        first_check.is_applicable = MagicMock(return_value=True)

        second_check = MagicMock()
        second_check.is_applicable = MagicMock(return_value=True)

        self.assertTrue(Verifier([first_check, second_check]).check_solution(self.context))
