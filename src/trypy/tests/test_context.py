from unittest import TestCase
from mock import patch, MagicMock
from trypy.context import Context


class ContextTest(TestCase):

    @patch('os.path.expanduser')
    def test_should_return_source_path(self, expanduser_mock):
        expanduser_mock.return_value = '/home/user'
        self.assertEqual('/home/user/trypy', Context().get_source_path())

    @patch('os.path.expanduser')
    def test_should_return_virtual_path(self, expanduser_mock):
        expanduser_mock.return_value = '/home/user'
        self.assertEqual('/home/user/tutorial-env', Context().get_virtual_path())

    @patch('trypy.context.Repo')
    @patch('os.path.expanduser')
    def test_should_fetch_branch_from_repo(self, expanduser_mock, repo_mock):
        expanduser_mock.return_value = '/home/user'
        Context().get_branch()

        repo_mock.assert_called_once_with('/home/user/trypy')

    @patch('imp.load_source')
    @patch('os.path.expanduser')
    def test_should_return_api_url_for_vagrant(self, expanduser_mock, load_source_mock):
        expanduser_mock.return_value = '/home/vagrant'
        load_source_mock.return_value = MagicMock(API_KEY='api-key-1234')

        self.assertEqual('http://localhost:7082/api/v0.1/api-key-1234/tiledata/', Context().get_api_url())

    @patch('imp.load_source')
    @patch('os.path.expanduser')
    def test_should_return_api_url_for_pylabs_user(self, expanduser_mock, load_source_mock):
        expanduser_mock.return_value = '/home/pyuser-15'
        load_source_mock.return_value = MagicMock(API_KEY='api-key-1234')

        self.assertEqual('http://localhost:7015/api/v0.1/api-key-1234/tiledata/', Context().get_api_url())
